racket-package
==============

This is a template generated using the `raco pkg new` command.

You can either use the `raco pkg new [your package name]` command directly or clone this repo.

**tasks**
- [ ] update this file [`README.md`](README.md)
- [ ] update [`.travis.yml`](.travis.yml) 
- [ ] update [`LICENSE.txt`](LICENSE.txt)
- [ ] update [`info.rkt`](info.rkt)
- [ ] update [`scribblings/racket-package.scrbl`](scribblings/racket-package.scrbl)
- [ ] update [`main.rkt`](main.rkt) to get coding.

For more details see [_Tutorial: Creating a Package_](https://blog.racket-lang.org/2017/10/tutorial-creating-a-package.html)

